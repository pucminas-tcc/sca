package br.com.mpx.sca.core.mvc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan
public class SpringWebConfig implements WebMvcConfigurer {

	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

	@Bean
	public DateFormatter dateFormatter() {
		return new DateFormatter();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/webjars/**", "/img/**", "/css/**", "/data/**", "/dist/**", "/vendor/**",
				"/less/**", "/js/**").addResourceLocations("classpath:/META-INF/resources/webjars/",
						"classpath:/static/img/", "classpath:/static/css/", "classpath:/static/data/",
						"classpath:/static/dist/", "classpath:/static/vendor/", "classpath:/static/less/",
						"classpath:/static/js/");
	}

}