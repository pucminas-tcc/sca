package br.com.mpx.sca.core.feign;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.form.FormEncoder;

@Configuration
public class FeignEncoderConfig {

	@Autowired
	private ObjectFactory<HttpMessageConverters> messageConverters;


	@Bean
	public Encoder encoder() {
		return new FormEncoder(new SpringEncoder(this.messageConverters));
	}
	
	public Decoder decoder() {
		return new SpringDecoder(messageConverters);
	}


}
