package br.com.mpx.sca.core.feign;

import org.springframework.context.annotation.Bean;

import feign.Contract;


public class FeignConfiguration {
	@Bean
	public Contract feignContract() {
		return new feign.Contract.Default();
	}

}
