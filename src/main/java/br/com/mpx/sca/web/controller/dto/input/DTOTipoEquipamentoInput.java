package br.com.mpx.sca.web.controller.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOTipoEquipamentoInput {
	
	private String detalhe;
	private String marca;
	private String modelo;
	private String serie;

}
