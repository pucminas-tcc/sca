package br.com.mpx.sca.web.controller.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOEquipamentoInput {
	
	private String nome;
	private Double horimetro;
	private DTOTipoEquipamentoInput tipoEquipamento;

}
