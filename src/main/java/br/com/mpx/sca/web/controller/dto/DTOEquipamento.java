package br.com.mpx.sca.web.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DTOEquipamento {

	private String nome;
	private Double horimetro;
	private DTOTipoEquipamento tipoEquipamento;
	
}
