package br.com.mpx.sca.web.controller;

import java.io.IOException;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.mpx.sca.domain.clients.CredenciaisKeycloakApiCadastro;
import br.com.mpx.sca.domain.clients.KeycloakAuthClient;

@Controller
public class HomeController {

	@Value("${url.logout}")
	private String keycloakUrlLogout;

	@Autowired
	KeycloakAuthClient keycloakClient;

	@Autowired
	private CredenciaisKeycloakApiCadastro credenciais;

	@GetMapping("/")
	public String home(ModelMap model, HttpServletRequest request) {

		model.addAttribute("usuario", getKeycloakSecurityContext(request).getToken().getName());

		String tokenApiCadastro = keycloakClient.getTokenApiCadastro(credenciais);

		String token = extractToken(tokenApiCadastro);

		request.getSession().setAttribute("token", token);

		return "index";
	}

	@GetMapping(path = "/logout")
	public void logout(HttpServletResponse response, HttpSession session) throws IOException {
		session.invalidate();
		response.sendRedirect(keycloakUrlLogout);
	}

	private KeycloakSecurityContext getKeycloakSecurityContext(HttpServletRequest request) {
		return (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
	}

	private String extractToken(String token) {
		try {
			JSONParser parser = new JSONParser(token);

			LinkedHashMap<String, Object> mapJson = parser.parseObject();

			return String.valueOf(mapJson.get("access_token"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
