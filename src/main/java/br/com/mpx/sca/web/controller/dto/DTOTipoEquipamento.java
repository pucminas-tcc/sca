package br.com.mpx.sca.web.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DTOTipoEquipamento {
	
	private String detalhe;
	private String marca;
	private String modelo;
	private String serie;
	
}
