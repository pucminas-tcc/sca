package br.com.mpx.sca.web.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.mpx.sca.domain.clients.EquipamentoClient;
import br.com.mpx.sca.web.controller.dto.DTOEquipamento;

@Controller
@RequestMapping("/equipamentos")
public class EquipamentoController {

	@Autowired
	private EquipamentoClient client;

	@Value("${keycloak.auth-server-url}")
	String keycloakAuthServerUrl;

	@Value("${url.api-cadastro-ativos}")
	private String uriApiCadastroAtivos;

	@GetMapping
	public ModelAndView home(ModelMap model, HttpSession session) {
		
		String token = String.valueOf(session.getAttribute("token"));

		List<DTOEquipamento> equipamentos = client.getEquipamentos("bearer " + token);

		model.addAttribute("equipamentos", equipamentos);

		return new ModelAndView("equipamentos/equipamentos", model);
	}

	@GetMapping("/novo")
	public ModelAndView cadastrarNovo(ModelMap model) {
		model.addAttribute("dtoEquipamento", new DTOEquipamento());

		return new ModelAndView("equipamentos/equipamentos-include", model);
	}

	@PostMapping
	public String cadastrar(@ModelAttribute("dtoEquipamento") DTOEquipamento dto, HttpSession session) {
		String token = String.valueOf(session.getAttribute("token"));

		this.postEquipamento(token, dto);

		return "redirect:/equipamentos";
	}

	private void postEquipamento(String token, DTOEquipamento dto) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add(HttpHeaders.AUTHORIZATION, "bearer " + token);

		HttpEntity<DTOEquipamento> request = new HttpEntity<DTOEquipamento>(dto, headers);

		String path = "/ativos/equipamentos";
		DTOEquipamento result = restTemplate.postForObject(uriApiCadastroAtivos+path, request, DTOEquipamento.class);

		System.out.println(result);
	}

}
