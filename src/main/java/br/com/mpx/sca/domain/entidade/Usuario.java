package br.com.mpx.sca.domain.entidade;

import lombok.Data;

@Data
public class Usuario {
	
	private String nome;
	private String email;
}
