package br.com.mpx.sca.domain.clients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredenciaisKeycloakApiCadastro {	
	@Value("${USER_API_CADASTRO_ATIVOS}")
	private String username;
	
	@Value("${PASSWORD_API_CADASTRO_ATIVOS}")
	private String password;
	
	@Value("${grant-type.cadastro.client-id}")	
	private String client_id;	

	@Value("${grant-type.cadastro.secrete-api}")
	private String grant_type;
	
	@Value("${KEY_CLOAK_API_CADASTRO_SECRET}")
	private String client_secret;
}
