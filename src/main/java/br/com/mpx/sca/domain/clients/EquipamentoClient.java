package br.com.mpx.sca.domain.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.mpx.sca.web.controller.dto.DTOEquipamento;
import feign.Headers;



@FeignClient(name = "equipamentos", url = "https://api-cadastro-ativos.herokuapp.com/ativos")
public interface EquipamentoClient {
	String str = "";

	@GetMapping(path = "/equipamentos", produces = MediaType.APPLICATION_JSON_VALUE)
	@Headers("Content-Type: application/json")
	List<DTOEquipamento> getEquipamentos(@RequestHeader(value = "Authorization", required = true) String token);

	@PostMapping(
			path = "/equipamentos",
			produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Content-Type: Content-Type: application/json")
	DTOEquipamento postEquipamento(
			@RequestHeader(value = "Authorization", required = true) String token, @RequestBody DTOEquipamento dtoEquipamento);
}
