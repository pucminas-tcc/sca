package br.com.mpx.sca.domain.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.mpx.sca.core.feign.FeignEncoderConfig;

@FeignClient(
		name = "keycloak",
		url = "https://keycloak-sica.herokuapp.com/auth/realms/sca-internet/protocol/openid-connect/token",
		configuration = FeignEncoderConfig.class)
public interface KeycloakAuthClient {

	@PostMapping(
			headers = "Content-Type: application/x-www-form-urlencoded",
			consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	String getTokenApiCadastro(@RequestBody CredenciaisKeycloakApiCadastro credenciais);

}
